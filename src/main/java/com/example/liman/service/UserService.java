package com.example.liman.service;

import com.example.liman.dto.LoginRequestDTO;
import com.example.liman.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    User addUsers(User user);

//    User getUsers(Long id) ;

    

    void deleteByUser(Long id);

    Long updateUsers(java.lang.Long id, User user);

    User login(LoginRequestDTO loginRequest);

   User getUsersById(Long id);


//    User login(UserDto userDto) throws UserNotFoundException;
}
