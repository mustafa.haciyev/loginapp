package com.example.liman.service.impl;

import com.example.liman.dto.LoginRequestDTO;
import com.example.liman.entity.User;
import com.example.liman.repo.UserRepository;
import com.example.liman.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User addUsers(User user) {
        return userRepository.save(user);
    }

    //    @Override
//    public User getUsers(Long id)  {
//        if (userRepository != null){
//            userRepository.findById(id).get();
//        } else {
//            throw new UserNotFoundException("User not found");
//        }
//        return null;
//    }
    @Override
    public User getUsersById(Long id) {
        return userRepository.findById(id).get();
    }


    @Override
    public Long updateUsers(Long id, User user) {
        if (userRepository.findById(id).isPresent()) {
            User user1 = userRepository.findById(id).get();
            user1.setName(user.getName());
            user1.setSurname(user.getSurname());
            user1.setMail(user.getMail());
            user1.setPassword(user.getPassword());
            userRepository.save(user1);
        } else {
            throw new RuntimeException("User not found " + id + " id");
        }
        return id;
    }

    public User login(LoginRequestDTO loginRequest) {
        String email = loginRequest.getMail();
        String password = loginRequest.getPassword();

        User user = userRepository.findByMail(email);

        if (user == null) {
            return null;
        }
        if (!user.getPassword().equals(password)) {
            return null;
        }
        return user;
    }


    @Override
    public void deleteByUser(Long id) {
        userRepository.deleteById(id);
    }
}
