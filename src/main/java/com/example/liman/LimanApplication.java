package com.example.liman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LimanApplication {

	public static void main(String[] args) {
		SpringApplication.run(LimanApplication.class, args);
	}

}
