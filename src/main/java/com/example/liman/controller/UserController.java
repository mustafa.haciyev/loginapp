package com.example.liman.controller;

import com.example.liman.entity.User;
import com.example.liman.exception.UserNotFoundException;
import com.example.liman.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @GetMapping
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

//    @GetMapping("/{id}")
//    public User getUsers(@PathVariable Long id) {
//       return userService.getUsers(id);
//
//    }

    @GetMapping("{id}")
    public User getUserById(@PathVariable Long id){
        return userService.getUsersById(id);
    }

    @PostMapping("/adduser")
    public User addUser(@RequestBody User user){
        return userService.addUsers(user);
    }

    @PutMapping("/{id}")
    public Long updateUser(@PathVariable Long id,@RequestBody User user){
        return userService.updateUsers(id,user);
    }

    @DeleteMapping("/{id}")
    public void deleteUsers(@PathVariable Long id){
        userService.deleteByUser(id);
    }

}
