package com.example.liman.controller;

import com.example.liman.dto.LoginRequestDTO;
import com.example.liman.entity.User;
import com.example.liman.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class LoginController {
    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginRequestDTO loginRequest) {
        User user = userService.login(loginRequest);

        if (user != null) {
            return ResponseEntity.ok("Login success!");
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login failed");
        }
    }
}
