package com.example.liman.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "user_name")
    String name;
    @Column(name = "user_surname")
    String surname;
    @Column(name = "user_mail")
    String mail;
    @Column(name = "user_password")
    String password;
}

