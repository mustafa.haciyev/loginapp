package com.example.liman.dto;

import lombok.Data;

@Data
public class LoginRequestDTO {
    private String mail;
    private String password;
}
